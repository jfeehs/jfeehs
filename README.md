## Josh's README

Hi, I'm Josh Feehs - a Senior Security Engineer for our Red Team. This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before.

Please feel free to contribute to this page by opening a merge request.

## Related pages

Here are some resources to learn more about our team:

- [GitLab Handbook - Red Team](https://about.gitlab.com/handbook/security/security-operations/red-team/): An overview of our team.
- [Red Team Public](https://gitlab.com/gitlab-com/gl-security/security-operations/redteam/redteam-public): Various resources we'd like to share with the community.

## About me

Here are some things about me:
* I've spent my whole career at the intersection of red teaming and offensive tool development. I really enjoy finding ways to break things in order to help make them better, and writing code that makes that process easier.
* I believe that writing is the most important part of hacking. Doing cool hacks doesn't make anything more secure if you can't communicate the problems, because nobody will know how to fix them.

## Communicating with me

- As a hacker, I'm constantly looking for corner cases. This means that I often over-explain something, or give explanations that account for corner cases that don't actually matter. I'm working to improve, but if we're talking and I veer off into corner cases, please feel free to stop me and help us re-focus.
- I would really appreciate any feedback that you think might help me do my job better. I won't take direct and honest feedback personally, so please reach out any time.
